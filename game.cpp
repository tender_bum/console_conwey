#include "game.h"
#include <cstdlib>
#include <iostream>
#include <vector>

game_::game_() {
  for (int x = 0; x < Game::HEIGHT; ++x)
    for (int y = 0; y < Game::WIDTH; ++y)
      _matrix[x][y] = (size_t)(rand() % 2);
}

void game_::update() {
  for (int x = 0; x < Game::HEIGHT; ++x)
    for (int y = 0; y < Game::WIDTH; ++y) {
      int nbs = get_neighbors(x, y);
      if (!_matrix[x][y] && nbs == 3)
        _buffer[x][y] = true;
      if (_matrix[x][y] && nbs != 2 && nbs != 3)
        _buffer[x][y] = false;
    }
  for (int x = 0; x < Game::HEIGHT; ++x)
    for (int y = 0; y < Game::WIDTH; ++y)
      _matrix[x][y] = _buffer[x][y];
}

void game_::redraw() {
#ifdef _WIN32
  system("cls");
#else
  system("clear");
#endif
  for (int x = 0; x < Game::HEIGHT; ++x) {
    for (int y = 0; y < Game::WIDTH; ++y)
      if (_matrix[x][y])
        std::cout << "██";
      else
        std::cout << "  ";
    std::cout << '\n';
  }
}

// clang-format off
std::array<std::pair<int, int>, 8> MOVES{ {
    { 1, 0 },
    { -1, 0 },
    { 0, 1 },
    { 0, -1 },
    { 1, 1 },
    { 1, -1 },
    { -1, 1 },
    { -1, -1 }
}};
// clang-format on

int game_::get_neighbors(int x, int y) {
  int cnt = 0;
  for (auto &dir : MOVES) {
    int xx = ((x + dir.first) % Game::HEIGHT + Game::HEIGHT) % Game::HEIGHT;
    int yy = ((y + dir.second) % Game::WIDTH + Game::WIDTH) % Game::WIDTH;
    cnt += (_matrix[xx][yy] ? 1 : 0);
  }
  return cnt;
}
