#include "game.h"
#include <cstdlib>
#include <ctime>
#include <thread>

int exec() {
  game_ game;
  while (true) {
    game.update();
    game.redraw();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
}

int main() { return exec(); }
