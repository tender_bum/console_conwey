#ifndef _GAME_H_
#define _GAME_H_

#include <array>

namespace Game {

const size_t WIDTH = 110;
const size_t HEIGHT = 80;

}; // namespace Game

class game_ {
public:
  game_();

  void update();

  void redraw();

private:
  std::array<std::array<bool, Game::WIDTH>, Game::HEIGHT> _matrix;
  std::array<std::array<bool, Game::WIDTH>, Game::HEIGHT> _buffer;

  int get_neighbors(int x, int y);
};

#endif // _GAME_H_
